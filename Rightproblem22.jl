### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# ╔═╡ 22c8b072-b46f-11ec-119c-a5b5e3e802f8
md"# installing packages required"

# ╔═╡ b15a1a8d-2ac3-4bcf-8655-b1eb2941a4e7
using Pkg;
Pkg.add(["Flux", "ProgressMeter", "BSON", "CUDA","MLDatasets", "TensorBoardLogger"];
GC.gc();


# ╔═╡ ac8a9e74-804e-4a35-8dd3-1ec9d9d634f4
Pkg.add(["FileIO", "Images", "ImageMagick", "PlutoUI"]);
Gc.gc();

# ╔═╡ db063086-ac33-4670-bad4-e4201e2f52cd
using Flux 
@epoch
using Flux.Data: DataLoader
using Flux.Optimise: Optimiser, WeightDecay
using Flux.Losses: logitcrossentropy
using Flux: onehotbatch, onecold
using Logging: with_logger
using Statistics, Random
using TensorBoardLogger: TBLogger, tb_overwrite, set_step!, set_step_increment!
using Images, FileIO
using PlutoUI
using ProgressMeter: 
import BSON
import MLDatasets


# ╔═╡ ff6eb4a4-aed5-48f7-bd21-3fe6b27eee61
md"# images"

# ╔═╡ bf8405d4-b93b-4fa1-bb98-8746d019e97d
data_dir = "chest_xray"
test_dir = "/chest_xray/test"
train_dir = "/chest_xray/train"
normal_pneumonia = ["NORMAL", "PNEUMONIA"]


# ╔═╡ 3766a8ea-f4a0-46be-b8c6-c313a0a35e62
imgsize = 32

# ╔═╡ 585df06d-68aa-4e54-8705-605c878b9339
trailabel = []

# ╔═╡ 269daf6a-f8ab-4b95-9ce4-4b3bcb116f84
testlabel = []

# ╔═╡ 369a73ee-dfa0-46c9-aa8a-17438f300fca
trainData = []

# ╔═╡ cfb993cb-c3f1-4652-add7-e4ceedf69485
testData = []

# ╔═╡ 8409addf-aa0a-4d2a-b4e0-75d45290cbbf
function load_pneumonia_data(;T=float32, class, img_data, label, datadir, normal_pneumonia, imgsize)
   for np in normal_pneumonia
		dir = joinpath(Datadir, np)
		class = eachindex(np)
		
		for tt in readdir(dir)
			img_array = Images.load(joinpath(dir, tt))
			img_array2 = imgresize(img_array, (imgsize, imgsize))
			append!(img_data, [img_array2])
			append!(label, [class])
		end
	end
end


# ╔═╡ dd3bf6e3-b50e-4316-aa09-579b4cfb7c3c
function get_images(dir)
        files = readdir(dir; join=true)
        nimgs = length(files)
        imgdata = Array{T}(undef, (imsize..., 1, nimgs))
        Threads.@threads for (i,file) in collect(enumerate(files))
            img = load(file)
            imgdata = (T∘Gray).(imresize(img, imsize))
            imdata[1:imgsize[1],1:imgsize[2],1,i] = reshape(imgdata, (imsize[1],imsize[2],1))
        end
        return imgdata
    end
    

function get_data()

	xtest = [normal_test; pneumonia_test]
	xtrain = [normal_train; pneumonia_train]


	p_test_labels = labels(xtest, "PNEUMONIA", "pneumonia")
	normal_test_labels = labels(xtest, "NORMAL", "normal")

	p_train_labels = labels(xtrain, "PNEUMONIA", "pneumonia")
	normal_train_labels = labels(xtrain, "NORMAL", "normal")

	ytest = vcat(normal_test_labels, p_test_labels)
	ytrain = vcat(normal_train_labels, p_train_labels)
	
	xtest = [process_image.(x) for x in xtest]
	xtrain = [process_image.(x) for x in xtrain]
	
	Dict(:xtrain => xtrain, :xtest => xtest, :ytrain => ytrain, :ytest => ytest)
end


# ╔═╡ 574a0707-8531-431c-b64a-cf7ceb343dbf
md"#AlexNet model"

# ╔═╡ fe4d574b-9343-4afc-abcd-6ab8f9587a7a
function alexnet_model (; imgsize=(180,180,1), nclasses=2)
  return Chain(Conv((11, 11), 1 => 55, stride = 4, relu),
               MaxPool((3, 3), stride = 2),
               Conv((5, 5), 90 => 180, relu, pad = 2),
               MaxPool((3, 3), stride = 2),
               Conv((3, 3), 180 => 200, relu, pad = 1, stride = 1),
               Conv((3, 3), 200 => 200, relu, pad = 1, stride = 1),
               Conv((3, 3), 200 => 180, relu, pad = 1, stride = 1),
               MaxPool((3, 3), stride = 2),
        	   AdaptiveMeanPool((6,6)),                
		       flatten,
               Dropout(0.5),
               Dense(200,180 relu),
               Dropout(0.5),
               Dense(200, 200, relu),
               Dense(200, nclasses),
               x -> sigmoid.(x))
end



# ╔═╡ 57c85a4a-31d5-4d1a-96f3-2897d8b8f373
md"#alex loss function"

# ╔═╡ 95bfd10c-a9b1-4a7b-b5de-291265191f89
function loss(dataloader, model)
    l = 0f0
    for (x,y) in dataloader
        l += loss(ŷ, y) * size(x)[end]    
    end
   return l/length(dataloader)
end


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═22c8b072-b46f-11ec-119c-a5b5e3e802f8
# ╠═b15a1a8d-2ac3-4bcf-8655-b1eb2941a4e7
# ╠═ac8a9e74-804e-4a35-8dd3-1ec9d9d634f4
# ╠═db063086-ac33-4670-bad4-e4201e2f52cd
# ╠═ff6eb4a4-aed5-48f7-bd21-3fe6b27eee61
# ╠═bf8405d4-b93b-4fa1-bb98-8746d019e97d
# ╠═3766a8ea-f4a0-46be-b8c6-c313a0a35e62
# ╠═585df06d-68aa-4e54-8705-605c878b9339
# ╠═269daf6a-f8ab-4b95-9ce4-4b3bcb116f84
# ╠═369a73ee-dfa0-46c9-aa8a-17438f300fca
# ╠═cfb993cb-c3f1-4652-add7-e4ceedf69485
# ╠═8409addf-aa0a-4d2a-b4e0-75d45290cbbf
# ╠═dd3bf6e3-b50e-4316-aa09-579b4cfb7c3c
# ╠═574a0707-8531-431c-b64a-cf7ceb343dbf
# ╠═fe4d574b-9343-4afc-abcd-6ab8f9587a7a
# ╠═57c85a4a-31d5-4d1a-96f3-2897d8b8f373
# ╠═95bfd10c-a9b1-4a7b-b5de-291265191f89
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
